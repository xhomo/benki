# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is a micro web service to mimic a �Bank Account�.
  Through this web service, one can query about the balance, deposit money, and withdraw
  money. 
* Version 1.0.0

### How do I get set up? ###

* Import the maven source code into eclipse or an appropriate IDE that can interprate java code
* Ensure JAVA_HOME variable is set to jdk installation root folder and jdk bin path is also setup
* For deployment scenario: 
* i) Install the 'benki.sql' script into mysql dbms  
* ii) uncomment the spring config files, 'DatabaseConfig.java', 'RootConfig.java'
* iii) uncomment line 8 (\\'return new Class<?>[] { RootConfig.class };') in BenkiAppInitializer.java file
* iv) uncomment line 12 (\\'@PersistenceContext(unitName = "entityManagerFactory"')) in AbstractRepository.java file
* Note: This optional since tests dont necessarily need the app deployed
* While in the IDE of choice e.g. Eclipse, right click on the source test class -> run as -> junit test

### Who do I talk to? ###

* antonio.xhomo@gmail.com