package com.tala.benki.exception;

public class AccountNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8474247833647837941L;

	public AccountNotFoundException() {

	}

	public AccountNotFoundException(String message) {
		super(message);
	}

}
