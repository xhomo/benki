package com.tala.benki.exception;

/*handles max deposit per transaction exception*/
public class MaxWithdrawPerTxnException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6077884040134093705L;

	public MaxWithdrawPerTxnException() {

	}

	public MaxWithdrawPerTxnException(String message) {
		super(message);
	}

}
