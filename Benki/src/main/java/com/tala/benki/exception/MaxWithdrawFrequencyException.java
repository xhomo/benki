package com.tala.benki.exception;

/*handles max deposit frequency exception*/
public class MaxWithdrawFrequencyException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1906832728524725575L;

	public MaxWithdrawFrequencyException() {

	}

	public MaxWithdrawFrequencyException(String message) {
		super(message);
	}

}
