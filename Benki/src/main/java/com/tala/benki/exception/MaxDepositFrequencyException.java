package com.tala.benki.exception;

/*handles max deposit frequency exception*/
public class MaxDepositFrequencyException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4853303162656086505L;

	public MaxDepositFrequencyException() {

	}

	public MaxDepositFrequencyException(String message) {
		super(message);
	}

}
