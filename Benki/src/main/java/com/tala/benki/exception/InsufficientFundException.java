package com.tala.benki.exception;

public class InsufficientFundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6857698821648952133L;

	public InsufficientFundException() {

	}

	public InsufficientFundException(String message) {
		super(message);
	}

}
