package com.tala.benki.exception;

public class MaxDepositException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 355077044250772802L;

	public MaxDepositException() {

	}

	public MaxDepositException(String message) {
		super(message);
	}

}
