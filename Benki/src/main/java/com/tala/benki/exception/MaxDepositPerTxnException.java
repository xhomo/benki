package com.tala.benki.exception;

/*handles max deposit per transaction exception*/
public class MaxDepositPerTxnException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6678755418204802675L;

	public MaxDepositPerTxnException() {

	}

	public MaxDepositPerTxnException(String message) {
		super(message);
	}

}
