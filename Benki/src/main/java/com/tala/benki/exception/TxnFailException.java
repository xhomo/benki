package com.tala.benki.exception;

public class TxnFailException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3405803268458716940L;

	public TxnFailException() {

	}

	public TxnFailException(String message) {
		super(message);
	}

}
