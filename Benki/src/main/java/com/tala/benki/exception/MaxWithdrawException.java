package com.tala.benki.exception;

public class MaxWithdrawException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5588019675479874659L;

	public MaxWithdrawException() {

	}

	public MaxWithdrawException(String message) {
		super(message);
	}

}
