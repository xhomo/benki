package com.tala.benki.service.entity;

import com.tala.benki.domain.entity.masterdata.Transaction;
import com.tala.benki.service.BaseService;

public interface TransactionService extends BaseService<Transaction> {

}
