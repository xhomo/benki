package com.tala.benki.service.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.tala.benki.domain.dto.BalanceResponse;
import com.tala.benki.domain.dto.TxnInfoResponse;
import com.tala.benki.domain.entity.masterdata.Account;
import com.tala.benki.domain.entity.masterdata.Transaction;
import com.tala.benki.exception.AccountNotFoundException;
import com.tala.benki.exception.InsufficientFundException;
import com.tala.benki.exception.MaxDepositException;
import com.tala.benki.exception.MaxDepositFrequencyException;
import com.tala.benki.exception.MaxDepositPerTxnException;
import com.tala.benki.exception.MaxWithdrawException;
import com.tala.benki.exception.MaxWithdrawFrequencyException;
import com.tala.benki.exception.MaxWithdrawPerTxnException;
import com.tala.benki.service.BaseService;

public interface AccountService extends BaseService<Account> {
	public Account findByAccountNumber(String accNo) throws Exception;

	public BalanceResponse getBalance(String accNo) throws AccountNotFoundException, Exception;

	public TxnInfoResponse deposit(String accountNumber, BigDecimal amount, Date date)
			throws MaxDepositException, MaxDepositFrequencyException, MaxDepositPerTxnException,
			AccountNotFoundException, Exception;

	public TxnInfoResponse withdraw(String accountNumber, BigDecimal amount, Date date)
			throws MaxWithdrawException, MaxWithdrawFrequencyException, MaxWithdrawPerTxnException,
			InsufficientFundException, AccountNotFoundException, Exception;

}
