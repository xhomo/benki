package com.tala.benki.service.entity.impl;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tala.benki.domain.entity.masterdata.Transaction;
import com.tala.benki.repository.entity.TransactionRepository;
import com.tala.benki.service.entity.TransactionService;

@Service
@Transactional
public class TransactionServiceImpl implements TransactionService, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3105306746541598186L;
	@Autowired
	private TransactionRepository transactionRepository;

	@Override
	public boolean save(Transaction entity) throws Exception {
		return transactionRepository.save(entity);
	}

	@Override
	public Transaction update(Transaction entity) throws Exception {
		return transactionRepository.update(entity);
	}

	@Override
	public Transaction getById(long id) throws Exception {
		return transactionRepository.getById(id);
	}

	@Override
	public List<Transaction> getAll() throws Exception {
		return transactionRepository.getAll();
	}

	@Override
	public boolean deleteById(long id) throws Exception {
		return transactionRepository.deleteById(id);
	}

	@Override
	public boolean deleteAll() throws Exception {
		return transactionRepository.deleteAll();
	}

}
