package com.tala.benki.service.entity.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tala.benki.domain.dto.BalanceResponse;
import com.tala.benki.domain.dto.TxnInfoResponse;
import com.tala.benki.domain.entity.masterdata.Account;
import com.tala.benki.domain.entity.masterdata.Transaction;
import com.tala.benki.enums.EntityStatus;
import com.tala.benki.enums.TxnStatus;
import com.tala.benki.enums.TxnType;
import com.tala.benki.exception.AccountNotFoundException;
import com.tala.benki.exception.InsufficientFundException;
import com.tala.benki.exception.MaxDepositException;
import com.tala.benki.exception.MaxDepositFrequencyException;
import com.tala.benki.exception.MaxDepositPerTxnException;
import com.tala.benki.exception.MaxWithdrawException;
import com.tala.benki.exception.MaxWithdrawFrequencyException;
import com.tala.benki.exception.MaxWithdrawPerTxnException;
import com.tala.benki.exception.TxnFailException;
import com.tala.benki.repository.entity.AccountRepository;
import com.tala.benki.repository.entity.TransactionRepository;
import com.tala.benki.service.entity.AccountService;
import com.tala.benki.util.GlobalSettings;
import com.tala.benki.util.IDUtils;

@Service
@Transactional(rollbackOn = Exception.class)
public class AccountServiceImpl implements AccountService, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7268788138284737564L;
	@Autowired
	private AccountRepository accountRepository;
	@Autowired
	private TransactionRepository transactionRepository;

	@Override
	public BalanceResponse getBalance(String accountNo) throws AccountNotFoundException, Exception {
		try {
			List<Account> accounts = accountRepository.getByAccountNumber(accountNo);
			if (accounts.isEmpty()) {
				throw new AccountNotFoundException("Account does not exist");
			}
			Account account = accounts.get(0);
			BalanceResponse response = new BalanceResponse(account.getBalance(), account.getAccountNumber(),
					account.getAccountName());
			return response;
		} catch (AccountNotFoundException anfe) {
			throw anfe;
		} catch (Exception ex) {
			throw ex;
		}

	}

	@Override
	@Transactional(rollbackOn = { MaxDepositPerTxnException.class, MaxDepositFrequencyException.class,
			AccountNotFoundException.class, MaxDepositException.class })
	public TxnInfoResponse deposit(String accountNumber, BigDecimal amount, Date date)
			throws MaxDepositException, MaxDepositFrequencyException, MaxDepositPerTxnException,
			AccountNotFoundException, Exception {
		try {
			if (amount.doubleValue() > GlobalSettings.getMaxDepositPerTxn()) {
				throw new MaxDepositPerTxnException("Exceeded Maximum Deposit Per Transaction");
			}

			Account account = null;
			try {
				List<Account> list = accountRepository.getByAccountNumber(accountNumber);
				if (list.isEmpty())
					throw new AccountNotFoundException("Account does not exist");
				account = list.get(0);
			} catch (AccountNotFoundException anfe) {
				throw anfe;
			}

			BigDecimal cumulativeDepositAmt = transactionRepository.getCumulativeTxnAmountForTheDay(date,
					account.get_id(), TxnType.Deposit.getValue());
			if (cumulativeDepositAmt.add(amount).doubleValue() > GlobalSettings.getMaxDepositPerDay()) {
				throw new MaxDepositException("Exceeded Maximum Deposit Amount For Today");
			}

			int txnHits = transactionRepository.getDailyTransactionCount(date, account.get_id(),
					TxnType.Deposit.getValue());
			if (txnHits > GlobalSettings.getMaxDepositFrequency()) {
				throw new MaxDepositFrequencyException("Exceeded The Allowed Daily Deposit Transactions Count");
			}

			/*
			 * All checks passed: create transaction record, update account book
			 * balance
			 */
			Transaction txn = new Transaction();			
			txn.set_id(IDUtils.generateRandom());
			txn.setAccount(account.get_id());
			txn.setAmount(amount);
			txn.setDateCreated(date);
			txn.setDateLastUpdated(date);
			txn.setDescription("Deposit Amount worth of $" + amount + "to Account Name, " + account.getAccountName()
					+ " Account Number, " + accountNumber + " on date, " + date);
			txn.setStatus(EntityStatus.Approved.getValue());
			txn.setTransactionType(TxnType.Deposit.getValue());
			boolean status = transactionRepository.save(txn);

			if (status) {
				account = accountRepository.updateBalance(account.getAccountNumber(), amount);
				return new TxnInfoResponse(
						"Deposit Amount worth of $" + amount + ", to Account Number: " + accountNumber,
						TxnStatus.Success.getValue());
			} else {
				throw new TxnFailException("Transaction Failed!");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new TxnFailException("Transaction Failed!");
		}

	}

	@Override
	@Transactional(rollbackOn = { MaxWithdrawPerTxnException.class, MaxWithdrawFrequencyException.class,
			AccountNotFoundException.class, MaxWithdrawException.class, InsufficientFundException.class })
	public TxnInfoResponse withdraw(String accountNumber, BigDecimal amount, Date date)
			throws MaxWithdrawException, MaxWithdrawFrequencyException, MaxWithdrawPerTxnException,
			InsufficientFundException, AccountNotFoundException, Exception {
		try {
			if (amount.doubleValue() > GlobalSettings.getMaxWithdrawPerTxn()) {
				throw new MaxWithdrawPerTxnException("Exceeded Maximum Withdraw Per Transaction");
			}

			Account account = null;
			try {
				List<Account> list = accountRepository.getByAccountNumber(accountNumber);
				if (list.isEmpty())
					throw new AccountNotFoundException("Account does not exist");
				account = list.get(0);
			} catch (AccountNotFoundException anfe) {
				throw anfe;
			}
			// check for insufficient funds
			int r = account.getBalance().compareTo(amount);
			if (r == -1) {
				throw new InsufficientFundException("Insufficient Funds");
			}

			BigDecimal cumulativeWithdrawAmt = transactionRepository.getCumulativeTxnAmountForTheDay(date,
					account.get_id(), TxnType.Withdrawal.getValue());
			if (cumulativeWithdrawAmt.add(amount).doubleValue() > GlobalSettings.getMaxWithdrawPerDay()) {
				throw new MaxDepositException("Exceeded Maximum Withdraw Amount For Today");
			}

			int txnHits = transactionRepository.getDailyTransactionCount(date, account.get_id(),
					TxnType.Withdrawal.getValue());
			if (txnHits > GlobalSettings.getMaxWithdrawFrequency()) {
				throw new MaxWithdrawFrequencyException("Exceeded The Allowed Daily Withdrawal Transactions Count");
			}

			/*
			 * All checks passed: create transaction record, update account book
			 * balance
			 */
			Transaction txn = new Transaction(IDUtils.generateRandom());
			txn.setAccount(account.get_id());
			txn.setAmount(amount);
			txn.setDateCreated(date);
			txn.setDateLastUpdated(date);
			txn.setDescription("Withdraw Amount worth of $" + amount + "from Account Name, " + account.getAccountName()
					+ " Account Number, " + accountNumber + " on date, " + date);
			txn.setStatus(EntityStatus.Approved.getValue());
			txn.setTransactionType(TxnType.Withdrawal.getValue());

			boolean status = transactionRepository.save(txn);

			if (status) {
				account = accountRepository.updateBalance(account.getAccountNumber(), amount);
				return new TxnInfoResponse(
						"Withdraw Amount worth of $" + amount + ", from Account Number: " + accountNumber,
						TxnStatus.Success.getValue());
			} else {
				throw new TxnFailException("Transaction Failed!");
			}
		} catch (Exception ex) {
			throw new TxnFailException("Transaction Failed!");
		}

	}

	@Override
	public Account findByAccountNumber(String accNo) throws Exception {
		try {
			List<Account> accounts = accountRepository.getByAccountNumber(accNo);
			if (accounts == null)
				return null;
			return accounts.get(0);

		} catch (Exception ex) {
			throw ex;
		}
	}

	@Override
	public boolean save(Account entity) throws Exception {
		return accountRepository.save(entity);
	}

	@Override
	public Account update(Account entity) throws Exception {
		return accountRepository.update(entity);
	}

	@Override
	public Account getById(long id) throws Exception {
		return accountRepository.getById(id);
	}

	@Override
	public List<Account> getAll() throws Exception {
		return accountRepository.getAll();
	}

	@Override
	public boolean deleteById(long id) throws Exception {
		return accountRepository.deleteById(id);
	}

	@Override
	public boolean deleteAll() throws Exception {
		return accountRepository.deleteAll();
	}

}
