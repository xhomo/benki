package com.tala.benki.enums;
public enum TxnStatus {
	Success("Successful"),
	Fail("Failed");

    private final String value;

    private TxnStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}