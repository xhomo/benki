package com.tala.benki.enums;
public enum TxnType {
	Deposit(1),
	Withdrawal(2);

    private final int value;

    private TxnType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}