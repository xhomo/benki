package com.tala.benki.enums;
public enum EntityStatus {
	Inactive(0),
	Disabled(1),
    Approved(2),
    Rejected(3),
    Pending(4);

    private final int value;

    private EntityStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}