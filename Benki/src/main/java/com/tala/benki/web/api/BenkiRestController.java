package com.tala.benki.web.api;

import java.math.BigDecimal;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tala.benki.domain.dto.BalanceResponse;
import com.tala.benki.domain.dto.TxnInfoResponse;
import com.tala.benki.domain.entity.masterdata.Account;
import com.tala.benki.domain.entity.masterdata.Transaction;
import com.tala.benki.enums.TxnStatus;
import com.tala.benki.exception.AccountNotFoundException;
import com.tala.benki.exception.InsufficientFundException;
import com.tala.benki.exception.MaxDepositException;
import com.tala.benki.exception.MaxDepositFrequencyException;
import com.tala.benki.exception.MaxDepositPerTxnException;
import com.tala.benki.exception.MaxWithdrawException;
import com.tala.benki.exception.MaxWithdrawFrequencyException;
import com.tala.benki.exception.MaxWithdrawPerTxnException;
import com.tala.benki.service.entity.AccountService;
import com.tala.benki.service.entity.TransactionService;
import com.tala.benki.util.IDUtils;

@RestController
@RequestMapping("/benki")
public class BenkiRestController {

	private final Logger Log = LoggerFactory.getLogger(BenkiRestController.class);

	@Autowired
	AccountService accountService;

	@Autowired
	TransactionService transactionService;

	/* create new account */
	@RequestMapping(value = "/account", method = RequestMethod.POST)
	public ResponseEntity<TxnInfoResponse> createAccount(@RequestBody Account account) {
		Log.info("creating account " + account.toString());
		Account acc = null;
		TxnInfoResponse response = null;
		try {
			HttpStatus httpStatus = null;
			acc = accountService.findByAccountNumber(account.getAccountNumber());
			if (acc == null) {
				accountService.save(account);
				response = new TxnInfoResponse("Account successfully created", TxnStatus.Success.getValue(),0L);
				httpStatus = HttpStatus.CREATED;
			} else {
				response = new TxnInfoResponse("Account exists", TxnStatus.Fail.getValue());
				httpStatus = HttpStatus.BAD_REQUEST;
				Log.info("account " + account.toString() + " already exists");
			}
			return new ResponseEntity<TxnInfoResponse>(response, httpStatus);
		} catch (Exception ex) {
			response = new TxnInfoResponse(ex.getMessage(), TxnStatus.Fail.getValue());
			return new ResponseEntity<TxnInfoResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/* return outstanding balance */
	@RequestMapping(value = "/account/{accno}", method = RequestMethod.GET)
	public ResponseEntity<BalanceResponse> getAccountBalance(@PathVariable("accno") String accNo) {
		Log.info("getting account balance by account number:  " + accNo);
		BalanceResponse response = null;
		try {
			response = accountService.getBalance(accNo);
			ResponseEntity<BalanceResponse> responseEntity = new ResponseEntity<BalanceResponse>(response,
					HttpStatus.OK);
			return responseEntity;// new
									// ResponseEntity<BalanceResponse>(response,
									// HttpStatus.OK);
		} catch (AccountNotFoundException anfe) {
			Log.info("account:  " + accNo + "doesn't exist");
			return new ResponseEntity<BalanceResponse>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<BalanceResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/* deposit: credit the account with the specified amount */
	@RequestMapping(value = "/deposit/{accno}/{amount}", method = RequestMethod.PUT)
	public ResponseEntity<TxnInfoResponse> deposit(@PathVariable("accno") String accNo,
			@PathVariable("amount") BigDecimal amount) {
		Log.info("depositing amount " + amount + " into account number:  " + accNo);
		TxnInfoResponse response = null;
		Date today = new Date();
		try {
			response = accountService.deposit(accNo, amount, today);
		} catch (MaxDepositException mde) {
			Log.info("maximum deposit threshhold reached");
			response = new TxnInfoResponse(mde.getMessage(), TxnStatus.Fail.getValue());
			return new ResponseEntity<TxnInfoResponse>(response, HttpStatus.BAD_REQUEST);
		} catch (MaxDepositFrequencyException mfe) {
			Log.info("maximum deposit frequency threshhold exceeded");
			response = new TxnInfoResponse(mfe.getMessage(), TxnStatus.Fail.getValue());
			return new ResponseEntity<TxnInfoResponse>(response, HttpStatus.BAD_REQUEST);
		} catch (MaxDepositPerTxnException mte) {
			Log.info("maximum deposit per transaction threshhold exceeded");
			response = new TxnInfoResponse(mte.getMessage(), TxnStatus.Fail.getValue());
			return new ResponseEntity<TxnInfoResponse>(response, HttpStatus.BAD_REQUEST);
		} catch (AccountNotFoundException anf) {
			Log.info("account doesn't exist!");
			response = new TxnInfoResponse(anf.getMessage(), TxnStatus.Fail.getValue());
			return new ResponseEntity<TxnInfoResponse>(response, HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			Log.info("generic error occured");
			response = new TxnInfoResponse("Generic error: " + e.getMessage(), TxnStatus.Fail.getValue());
			return new ResponseEntity<TxnInfoResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<TxnInfoResponse>(response, HttpStatus.OK);
	}

	/* withdraw: debit the account with the specified amount */
	@RequestMapping(value = "/withdraw/{accno}/{amount}", method = RequestMethod.PUT)
	public ResponseEntity<TxnInfoResponse> withdraw(@PathVariable("accno") String accNo,
			@PathVariable("amount") BigDecimal amount) {
		Log.info("withdrawing amount: " + amount + " from account: " + accNo);
		TxnInfoResponse response = null;
		try {
			response = accountService.withdraw(accNo, amount, new Date());
			Log.info("withdraw transaction successful!");
		} catch (MaxWithdrawException mwe) {
			Log.info("max withdrawal threshold reached ");
			response = new TxnInfoResponse(mwe.getMessage(), TxnStatus.Fail.getValue());
			return new ResponseEntity<TxnInfoResponse>(response, HttpStatus.BAD_REQUEST);
		} catch (MaxWithdrawFrequencyException mfe) {
			Log.info("max withdrawal frequency threshold reached ");
			response = new TxnInfoResponse(mfe.getMessage(), TxnStatus.Fail.getValue());
			return new ResponseEntity<TxnInfoResponse>(response, HttpStatus.BAD_REQUEST);
		} catch (MaxWithdrawPerTxnException mte) {
			Log.info("max withdrawal per transaction threshold reached ");
			response = new TxnInfoResponse(mte.getMessage(), TxnStatus.Fail.getValue());
			return new ResponseEntity<TxnInfoResponse>(response, HttpStatus.BAD_REQUEST);
		} catch (AccountNotFoundException anfe) {
			Log.info("account doesn't exist ");
			response = new TxnInfoResponse(anfe.getMessage(), TxnStatus.Fail.getValue());
			return new ResponseEntity<TxnInfoResponse>(response, HttpStatus.BAD_REQUEST);
		} catch (InsufficientFundException ife) {
			Log.info("insufficient funds error occured ");
			response = new TxnInfoResponse(ife.getMessage(), TxnStatus.Fail.getValue());
			return new ResponseEntity<TxnInfoResponse>(response, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			Log.info("generic error occured ");
			response = new TxnInfoResponse("Generic error: " + e.getMessage(), TxnStatus.Fail.getValue());
			return new ResponseEntity<TxnInfoResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<TxnInfoResponse>(response, HttpStatus.OK);
	}

}