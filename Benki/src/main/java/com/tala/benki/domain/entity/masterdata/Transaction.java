package com.tala.benki.domain.entity.masterdata;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Table;

import com.tala.benki.domain.entity.Entity;

@javax.persistence.Entity
@Table(name = "acc_txn")
public class Transaction extends Entity {
	private long account;
	private BigDecimal amount;
	@Column(name = "txn_type")
	private int transactionType;

	public Transaction() {
		super();
	}
	public Transaction(long _id) {
		this.set_id(_id);
	}

	public Transaction(long _id, Date dateCreated, int status, Date dateLastUpdated, String description, long account,
			BigDecimal amount, int transactionType) {
		super(_id, dateCreated, status, dateLastUpdated, description);
		this.account = account;
		this.amount = amount;
		this.transactionType = transactionType;
	}

	public Transaction(long account, BigDecimal amount, int transactionType) {
		this.account = account;
		this.amount = amount;
		this.transactionType = transactionType;
	}

	public long getAccount() {
		return account;
	}

	public void setAccount(long account) {
		this.account = account;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public int getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(int transactionType) {
		this.transactionType = transactionType;
	}

	@Override
	public String toString() {
		return "Transaction [account=" + account + ", amount=" + amount + ", transactionType=" + transactionType
				+ ", getAccount=" + getAccount() + ", getAmount()=" + getAmount() + ", getTransactionType()="
				+ getTransactionType() + "]";
	}

}
