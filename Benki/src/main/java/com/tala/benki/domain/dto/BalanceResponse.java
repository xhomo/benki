package com.tala.benki.domain.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.tala.benki.util.IDUtils;

public class BalanceResponse implements Serializable {

	private BigDecimal balance;
	private String accountnumber;
	private String accountname;

	public BalanceResponse() {
	}

	public BalanceResponse(BigDecimal balance, String accountnumber, String accountname) {
		this.balance = balance;
		this.accountnumber = accountnumber;
		this.accountname = accountname;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public String getAccountnumber() {
		return accountnumber;
	}

	public void setAccountnumber(String accountnumber) {
		this.accountnumber = accountnumber;
	}

	public String getAccountname() {
		return accountname;
	}

	public void setAccountname(String accountname) {
		this.accountname = accountname;
	}

	@Override
	public String toString() {
		return "{balance='" + balance + "', accountnumber='" + accountnumber + "', accountname='"
				+ accountname + "'}";
	}

}
