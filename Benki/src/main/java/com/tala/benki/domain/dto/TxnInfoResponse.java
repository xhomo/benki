package com.tala.benki.domain.dto;

public class TxnInfoResponse {
	private String errorInfo;
	private String txnStatus;
	private long txnId;

	public TxnInfoResponse(String errorInfo, String txnStatus, long txnId) {
		this.errorInfo = errorInfo;
		this.txnStatus = txnStatus;
		this.txnId = txnId;
	}

	public long getTxnId() {
		return txnId;
	}

	public void setTxnId(long txnId) {
		this.txnId = txnId;
	}

	public TxnInfoResponse(String errorInfo, String txnStatus) {
		this.errorInfo = errorInfo;
		this.txnStatus = txnStatus;
	}

	public String getErrorInfo() {
		return errorInfo;
	}

	public void setErrorInfo(String errorInfo) {
		this.errorInfo = errorInfo;
	}

	public String getTxnStatus() {
		return txnStatus;
	}

	public void setTxnStatus(String txnStatus) {
		this.txnStatus = txnStatus;
	}

	@Override
	public String toString() {
		return "{errorInfo='" + errorInfo + "', txnStatus='" + txnStatus + "'}";
	}

}
