package com.tala.benki.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class Entity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long _id;
	@Column(name = "date_created")
	private Date dateCreated;
	private int status;
	@Column(name = "date_last_updated")
	private Date dateLastUpdated;
	private String description;

	public Entity() {

	}

	public Entity(long _id, Date dateCreated, int status, Date dateLastUpdated, String description) {
		this._id = _id;
		this.dateCreated = dateCreated;
		this.status = status;
		this.dateLastUpdated = dateLastUpdated;
		this.description = description;
	}

	public long get_id() {
		return _id;
	}

	public void set_id(long _id) {
		this._id = _id;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getDateLastUpdated() {
		return dateLastUpdated;
	}

	public void setDateLastUpdated(Date dateLastUpdated) {
		this.dateLastUpdated = dateLastUpdated;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Entity [_id=" + _id + ", dateCreated=" + dateCreated + ", status=" + status + ", dateLastUpdated="
				+ dateLastUpdated + ", description=" + description + "]";
	}
}
