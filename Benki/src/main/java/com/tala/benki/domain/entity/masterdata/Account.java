package com.tala.benki.domain.entity.masterdata;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import com.tala.benki.domain.entity.Entity;

@javax.persistence.Entity
@NamedQueries({
	
	@NamedQuery(name = "Account.findByAccountNo", query = "SELECT a FROM Account a WHERE a.accountNumber = :accNo")

})
public class Account extends Entity {
	@Column(name = "name")
	private String accountName;
	@Column(name = "acc_no")
	private String accountNumber;
	@Column(name = "book_balance")
	private BigDecimal balance;

	public Account() {
		super();
	}

	public Account(long _id, String accountName, String accountNumber, Date dateCreated, Date dateLastUpdated,
			String description, int status, BigDecimal balance) {
		super(_id, dateCreated, status, dateLastUpdated, description);
		this.accountName = accountName;
		this.accountNumber = accountNumber;
		this.balance = balance;
	}

	public Account(String accountName, String accountNumber, BigDecimal balance) {
		this.accountName = accountName;
		this.accountNumber = accountNumber;
		this.balance = balance;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "Account [accountName=" + accountName + ", accountNumber=" + accountNumber + ", balance=" + balance
				+ "]";
	}
}
