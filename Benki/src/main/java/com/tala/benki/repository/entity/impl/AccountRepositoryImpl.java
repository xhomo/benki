package com.tala.benki.repository.entity.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.tala.benki.domain.entity.masterdata.Account;
import com.tala.benki.exception.AccountNotFoundException;
import com.tala.benki.repository.AbstractRepository;
import com.tala.benki.repository.entity.AccountRepository;

@Repository
public class AccountRepositoryImpl extends AbstractRepository<Account> implements AccountRepository, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2116988017312687543L;

	public AccountRepositoryImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
		this.entityClass = Account.class;
	}

	public AccountRepositoryImpl() {
		this.entityClass = Account.class;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Account> getByAccountNumber(String accountNo) throws AccountNotFoundException, Exception {
		try {
			Query query = (TypedQuery<Account>) entityManager.createNamedQuery("Account.findByAccountNo", entityClass);
			query.setParameter("accNo", accountNo.trim());
			List<Account> details = query.getResultList();
			if (details.isEmpty() || details == null) {
				throw new AccountNotFoundException("Account does not exist");
			} else {
				return details;
			}
		} catch (Exception ex) {
			throw ex;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Account updateBalance(String accountNo, BigDecimal amount) throws AccountNotFoundException, Exception {
		try {
			Query query = (TypedQuery<Account>) entityManager.createNamedQuery("Account.findByAccountNo", entityClass);
			query.setParameter("accNo", accountNo.trim());
			List<Account> details = query.getResultList();
			if (details.isEmpty() || details == null) {
				throw new AccountNotFoundException("The Account does not exist ");
			} else {
				Account account = details.get(0);
				account.setBalance(account.getBalance().add(amount));
				Account updatedAccount = update(account);

				return updatedAccount;
			}
		} catch (Exception ex) {
			throw ex;
		}
	}

}
