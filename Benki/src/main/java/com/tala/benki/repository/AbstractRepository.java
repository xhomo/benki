package com.tala.benki.repository;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.validation.constraints.NotNull;
import com.tala.benki.domain.entity.Entity;

public abstract class AbstractRepository<T extends Entity> {

	//@PersistenceContext(unitName = "entityManagerFactory")
	protected EntityManager entityManager;

	protected Class<?> entityClass;
	protected String entityName;

	// persist object
	public synchronized boolean save(@NotNull T entity) {
		boolean status = false;
		try {
			this.entityManager.persist(entity);
			status = true;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return status;
	}

	// merge object
	public synchronized T update(@NotNull T entity) {
		T updatedEntity = null;
		try {
			updatedEntity = this.entityManager.merge(entity);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return updatedEntity;
	}

	// find by primary key
	@SuppressWarnings("unchecked")
	public synchronized T getById(@NotNull long id) {
		try {
			return (T) this.entityManager.find(entityClass, id);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	// find all in the entity table
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public synchronized List<T> getAll() {
		try {
			CriteriaQuery cq = this.entityManager.getCriteriaBuilder().createQuery();
			cq.select(cq.from(entityClass));
			return this.entityManager.createQuery(cq).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	// delete using primary key
	public synchronized boolean deleteById(@NotNull long id) {
		boolean status = false;
		try {
			this.entityManager.remove(id);
			status = true;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return status;
	}

	// delete all record in the table
	public synchronized boolean deleteAll() {
		boolean status = false;
		try {
			entityManager.createQuery("DELETE FROM " + entityClass.getName()).executeUpdate();
			status = true;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return status;
	}

	// find by range(for tables with pagination)
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public synchronized List<Entity> findRange(@NotNull int[] range) {
		try {
			CriteriaQuery cq = this.entityManager.getCriteriaBuilder().createQuery();
			cq.select(cq.from(entityClass));
			javax.persistence.Query q = this.entityManager.createQuery(cq);
			q.setMaxResults(range[1] - range[0]);
			q.setFirstResult(range[0]);
			return q.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

}
