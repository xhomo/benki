package com.tala.benki.repository;

import java.util.List;

import com.tala.benki.domain.entity.Entity;

public interface BaseRepository<T extends Entity> {
	public boolean save(T entity) throws Exception;
	public T update(T entity) throws Exception;
	public T getById(long id) throws Exception;
	public List<T> getAll() throws Exception;
	public boolean deleteById(long id) throws Exception;
	public boolean deleteAll() throws Exception;
	
}