package com.tala.benki.repository.entity;

import java.math.BigDecimal;
import java.util.List;

import com.tala.benki.domain.entity.masterdata.Account;
import com.tala.benki.exception.AccountNotFoundException;
import com.tala.benki.repository.BaseRepository;

public interface AccountRepository extends BaseRepository<Account> {
	public List<Account> getByAccountNumber(String accNo) throws AccountNotFoundException, Exception;
	public Account updateBalance(String accNo, BigDecimal amount) throws AccountNotFoundException, Exception;
}
