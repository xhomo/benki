package com.tala.benki.repository.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.tala.benki.domain.entity.masterdata.Transaction;
import com.tala.benki.repository.BaseRepository;

public interface TransactionRepository extends BaseRepository<Transaction> {
	public BigDecimal getCumulativeTxnAmountForTheDay(Date dateCreated, long account, int transactionType)
			throws Exception;

	public int getDailyTransactionCount(Date date, long account, int transactionType) throws Exception;
}
