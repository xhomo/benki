package com.tala.benki.repository.entity.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import com.tala.benki.domain.entity.masterdata.Transaction;
import com.tala.benki.exception.AccountNotFoundException;
import com.tala.benki.repository.AbstractRepository;
import com.tala.benki.repository.entity.TransactionRepository;

@Repository
public class TransactionRepositoryImpl extends AbstractRepository<Transaction>
		implements TransactionRepository, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3538101716319204738L;

	public TransactionRepositoryImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
		this.entityClass = Transaction.class;
		// TODO Auto-generated constructor stub
	}

	public TransactionRepositoryImpl() {
		this.entityClass = Transaction.class;
		// TODO Auto-generated constructor stub
	}

	@Override
	public BigDecimal getCumulativeTxnAmountForTheDay(Date dateCreated, long account, int transactionType)
			throws Exception {
		try {
			Query q = entityManager.createNativeQuery(
					"SELECT SUM(t.amount) FROM Transaction t WHERE t.dateCreated = :dateCreated AND t.account = :account AND t.transactionType = :transactionType");
			q.setParameter("dateCreated", dateCreated);
			q.setParameter("account", account);
			q.setParameter("transactionType", transactionType);
			// Transactions object array
			Object[] txns = (Object[]) q.getSingleResult();
			BigDecimal amount = null;
			if (txns.length > 0) {
				amount = new BigDecimal((String) txns[0]);
			} else {
				amount = new BigDecimal(0);
			}
			return amount;
		} catch (Exception ex) {
			throw ex;
		}
	}

	@Override
	public int getDailyTransactionCount(Date dateCreated, long account, int transactionType) throws Exception {
		try {
			Query q = entityManager.createNativeQuery(
					"SELECT COUNT(t._id) FROM Transaction t WHERE t.dateCreated = :dateCreated AND t.account = :account AND t.transactionType = :transactionType");
			q.setParameter("dateCreated", dateCreated);
			q.setParameter("account", account);
			q.setParameter("transactionType", transactionType);

			Object[] txns = (Object[]) q.getSingleResult();
			int count = 0;
			if (txns.length > 0) {
				count = (int) txns[0];
			}
			return count;
		} catch (Exception ex) {
			throw ex;
		}
	}

}
