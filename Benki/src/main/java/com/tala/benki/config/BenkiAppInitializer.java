package com.tala.benki.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class BenkiAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {	
	@Override
	protected Class<?>[] getRootConfigClasses() {
		 //return new Class<?>[] { RootConfig.class };
		return new Class<?>[] { WebConfig.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return null;
	}

	@Override
	protected String[] getServletMappings() {
		// TODO Auto-generated method stub
		  return new String[] { "/" };
	}

}
