//package com.tala.benki.config;
//
//import javax.persistence.EntityManagerFactory;
//import javax.sql.DataSource;
//
//import org.apache.commons.dbcp2.BasicDataSource;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Primary;
//import org.springframework.context.annotation.PropertySource;
//import org.springframework.core.env.Environment;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.orm.hibernate4.HibernateExceptionTranslator;
//import org.springframework.orm.jpa.JpaTransactionManager;
//import org.springframework.orm.jpa.JpaVendorAdapter;
//import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
//import org.springframework.orm.jpa.vendor.Database;
//import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
//@Configuration
//@EnableTransactionManagement
//@EnableJpaRepositories(
//        entityManagerFactoryRef = "entityManagerFactory", 
//        transactionManagerRef = "transactionManager",
//        basePackages = { "com.tala.benki.repository.entity" })
//@PropertySource({
//    "classpath:application.properties"
//})
//public class DatabaseConfig {
//	private static final String PROPERTY_NAME_DATABASE_DRIVER = "db.driver";
//	private static final String PROPERTY_NAME_DATABASE_PASSWORD = "db.password";
//	private static final String PROPERTY_NAME_DATABASE_URL = "db.url";
//	private static final String PROPERTY_NAME_DATABASE_USERNAME = "db.username";
//
//	@Autowired
//	private Environment environment;
//
//	@Bean(destroyMethod = "close", name = "dataSource")
//	public BasicDataSource dataSource() { /* pooled */
//		final BasicDataSource dataSource = new BasicDataSource();
//		dataSource.setDriverClassName(this.environment.getRequiredProperty(PROPERTY_NAME_DATABASE_DRIVER));
//		dataSource.setUrl(this.environment.getRequiredProperty(PROPERTY_NAME_DATABASE_URL));
//		dataSource.setUsername(this.environment.getRequiredProperty(PROPERTY_NAME_DATABASE_USERNAME));
//		dataSource.setPassword(this.environment.getRequiredProperty(PROPERTY_NAME_DATABASE_PASSWORD));
//		dataSource.setInitialSize(5);
//		return dataSource;
//	}
//
//	@Bean(name = "entityManagerFactory")
//	@Primary
//	public LocalContainerEntityManagerFactoryBean entityManagerFactory(@Qualifier("dataSource")final DataSource dataSource,
//			final JpaVendorAdapter jpaVendorAdapter) {
//		final LocalContainerEntityManagerFactoryBean emfb = new LocalContainerEntityManagerFactoryBean();
//		emfb.setDataSource(dataSource);
//		emfb.setJpaVendorAdapter(jpaVendorAdapter);
//		emfb.setPersistenceXmlLocation("classpath:META-INF/persistence.xml");
//		return emfb;
//	}
//
//	@Bean
//	public HibernateExceptionTranslator hibernateExceptionTranslator() {
//		return new HibernateExceptionTranslator();
//	}
//
//	@Bean
//	public HibernateJpaVendorAdapter jpaVendorAdapter() {
//		final HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
//		adapter.setDatabase(Database.MYSQL);
//		adapter.setShowSql(true);
//		adapter.setGenerateDdl(true);
//		adapter.setDatabasePlatform("org.hibernate.dialect.MySQLDialect");
//		return adapter;
//	}
//
//
//	@Bean(name = "transactionManager")
//	public JpaTransactionManager transactionManager(@Qualifier("entityManagerFactory")final EntityManagerFactory entityManagerFactory) {
//		final JpaTransactionManager transactionManager = new JpaTransactionManager();
//		transactionManager.setEntityManagerFactory(entityManagerFactory);
//		return new JpaTransactionManager();
//	}
//}
