//package com.tala.benki.config;
//
//import org.springframework.context.MessageSource;
//import org.springframework.context.annotation.AdviceMode;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.ComponentScan.Filter;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.FilterType;
//import org.springframework.context.annotation.Import;
//import org.springframework.context.annotation.Primary;
//import org.springframework.context.annotation.PropertySource;
//import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
//import org.springframework.context.support.ResourceBundleMessageSource;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//import org.springframework.validation.Validator;
//import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//
//@Configuration
//@ComponentScan(
//        basePackages = {
//                "com.tala.benki",
//                "com.tala.benki.service"
//        		},
//        excludeFilters = {
//                @Filter(
//                        type = FilterType.ANNOTATION) })//,value = EnableWebMvc.class
//@EnableJpaRepositories(
//        basePackages = {
//                "com.tala.benki.repository",
//                "com.tala.benki.repository.entity"})
//@EnableTransactionManagement(
//        mode = AdviceMode.PROXY)
//@PropertySource({
//        "classpath:application.properties"
//	})
//@Import(
//        value = {
//                DatabaseConfig.class,
//                WebConfig.class
//                })
//
//public class RootConfig {
//
//    @Bean
//    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
//        return new PropertySourcesPlaceholderConfigurer();
//    }
//
//    @Bean
//    @Primary
//    public MessageSource messageSource() {
//        final ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
//        messageSource.setBasename("messages");
//        return messageSource;
//    }
//
//    @Bean
//    public Validator validator() {
//        return new LocalValidatorFactoryBean();
//    }/*enable Validator support*/
//
//    @Bean
//    public ObjectMapper jsonMapper() {
//        return new ObjectMapper();
//    }/*enable json marshal & unmarshal support*/
//}
