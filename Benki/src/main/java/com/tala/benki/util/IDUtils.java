package com.tala.benki.util;

import java.security.SecureRandom;
import java.util.Random;

public class IDUtils {
	public static long generateRandom() {
		long seed = System.currentTimeMillis();
		Random random = new SecureRandom();
		random.setSeed(seed);
		return Math.abs(random.nextLong());
	}

}
