package com.tala.benki.util;

public class GlobalSettings {
	private static final double MAX_DEPOSIT_PER_DAY = 150000;
	private static final double MAX_WITHDRAW_PER_DAY = 50000;
	private static final double MAX_DEPOSIT_PER_TXN = 40000;
	private static final double MAX_WITHDRAW_PER_TXN = 20000;
	private static final int MAX_DEPOSIT_FREQUENCY = 4;
	private static final int MAX_WITHDRAW_FREQUENCY = 3;

	public static double getMaxDepositPerDay() {
		return MAX_DEPOSIT_PER_DAY;
	}

	public static double getMaxWithdrawPerDay() {
		return MAX_WITHDRAW_PER_DAY;
	}

	public static double getMaxDepositPerTxn() {
		return MAX_DEPOSIT_PER_TXN;
	}

	public static double getMaxWithdrawPerTxn() {
		return MAX_WITHDRAW_PER_TXN;
	}

	public static int getMaxDepositFrequency() {
		return MAX_DEPOSIT_FREQUENCY;
	}

	public static int getMaxWithdrawFrequency() {
		return MAX_WITHDRAW_FREQUENCY;
	}

}
