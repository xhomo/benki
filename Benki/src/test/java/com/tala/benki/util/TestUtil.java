package com.tala.benki.util;

import java.math.BigDecimal;
import java.util.Date;

import com.tala.benki.domain.dto.BalanceResponse;
import com.tala.benki.domain.entity.masterdata.Account;
import com.tala.benki.domain.entity.masterdata.Transaction;
import com.tala.benki.enums.EntityStatus;
import com.tala.benki.enums.TxnType;

public class TestUtil {
	public static Account createAccount(String accountName, String accountNumber, BigDecimal balance) {
		Account account = new Account(accountName, accountNumber, balance);
		account.set_id(IDUtils.generateRandom());
		Date today = new Date();
		account.setDateCreated(today);
		account.setDateLastUpdated(today);
		account.setDescription("ACC_" + accountName + "_" + accountNumber);
		account.setStatus(EntityStatus.Approved.getValue());
		return account;
	}

	public static Transaction createTxn(long accountId, BigDecimal amount, int transactionType) {
		Transaction txn = new Transaction();
		Date today = new Date();
		txn.set_id(IDUtils.generateRandom());
		txn.setAccount(accountId);
		txn.setAmount(amount);
		txn.setDateCreated(today);
		txn.setDateLastUpdated(today);
		txn.setDescription("TXN_" + accountId + "_" + today + "_" + amount);
		txn.setStatus(EntityStatus.Approved.getValue());
		txn.setTransactionType(transactionType);
		return txn;
	}
	public static BalanceResponse createBalanceResponse(BigDecimal balance, String accountNumber, String accountName) {
		return new BalanceResponse(balance,accountNumber, accountName);
		
	}

}
