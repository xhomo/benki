package com.tala.benki.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.*;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.tala.benki.domain.dto.BalanceResponse;
import com.tala.benki.domain.dto.TxnInfoResponse;
import com.tala.benki.domain.entity.masterdata.Account;
import com.tala.benki.domain.entity.masterdata.Transaction;
import com.tala.benki.enums.TxnStatus;
import com.tala.benki.enums.TxnType;
import com.tala.benki.repository.entity.AccountRepository;
import com.tala.benki.repository.entity.TransactionRepository;
import com.tala.benki.service.entity.AccountService;
import com.tala.benki.service.entity.impl.AccountServiceImpl;
import com.tala.benki.util.TestUtil;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {
	@Mock
	private AccountRepository accountRepository;
	@Mock
	private TransactionRepository transactionRepository;
	@InjectMocks
	private AccountService accountService = new AccountServiceImpl();

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void testGetBalance() throws Exception {
		String accountNumber = "508090345600";
		String accountName = "Antony Nzomo";
		BigDecimal openingBalance = new BigDecimal(500000);

		List<Account> accounts = Arrays.asList(TestUtil.createAccount(accountName, accountNumber, openingBalance));
		when(accountRepository.getByAccountNumber(accountNumber)).thenReturn(accounts);

		BalanceResponse balanceResponse = accountService.getBalance(accountNumber);

		assertBalanceResponseEquals(balanceResponse, accountName, accountNumber, openingBalance);
	}

	@Test
	public void testDeposit() throws Exception {
		Date today = new Date();
		int depositTxnCount = 3;// daily transaction count
		boolean depositTxnStatus = true;
		String accountNumber = "508090345600";
		String accountName = "Antony Nzomo";
		BigDecimal openingBalance = new BigDecimal(500000);
		BigDecimal depositAmount = new BigDecimal(5000);
		BigDecimal dailyCummulativeDepositAmt = new BigDecimal(25000);
		List<Account> accounts = Arrays.asList(TestUtil.createAccount(accountName, accountNumber, openingBalance));
		
		
		when(accountRepository.getByAccountNumber(accountNumber)).thenReturn(accounts);
		when(transactionRepository.getCumulativeTxnAmountForTheDay(today, accounts.get(0).get_id(),
				TxnType.Deposit.getValue())).thenReturn(dailyCummulativeDepositAmt);
		when(transactionRepository.getDailyTransactionCount(today, accounts.get(0).get_id(),
				TxnType.Deposit.getValue())).thenReturn(depositTxnCount);
		when(transactionRepository.save(any(Transaction.class))).thenReturn(depositTxnStatus);
		when(accountRepository.updateBalance(accountNumber, depositAmount)).thenReturn(accounts.get(0));

		TxnInfoResponse txnInfoResponse = accountService.deposit(accountNumber, depositAmount,today);

		Assert.assertEquals(txnInfoResponse.getTxnStatus(), TxnStatus.Success.getValue());
		verify(accountRepository, times(1)).updateBalance(accountNumber, depositAmount);
		verify(transactionRepository, times(1)).save(any(Transaction.class));

	}
	@SuppressWarnings("deprecation")
	@Test
	public void testWithDraw() throws Exception {
		Date today = new Date();
		int withdrawalTxnCount = 3;// daily transaction count
		boolean withdrawalTxnStatus = true;
		String accountNumber = "508090345600";
		String accountName = "Antony Nzomo";
		BigDecimal openingBalance = new BigDecimal(500000);
		BigDecimal withdrawalAmount = new BigDecimal(5000);
		BigDecimal dailyCummulativeWithdrawalAmt = new BigDecimal(25000);
		List<Account> accounts = Arrays.asList(TestUtil.createAccount(accountName, accountNumber, openingBalance));
		
		when(accountRepository.getByAccountNumber(accountNumber)).thenReturn(accounts);
		when(transactionRepository.getCumulativeTxnAmountForTheDay(today, accounts.get(0).get_id(),
				TxnType.Withdrawal.getValue())).thenReturn(dailyCummulativeWithdrawalAmt);
		when(transactionRepository.getDailyTransactionCount(today, accounts.get(0).get_id(),
				TxnType.Withdrawal.getValue())).thenReturn(withdrawalTxnCount);
		when(transactionRepository.save(any(Transaction.class))).thenReturn(withdrawalTxnStatus);
		when(accountRepository.updateBalance(accountNumber, withdrawalAmount)).thenReturn(accounts.get(0));

		TxnInfoResponse txnInfoResponse = accountService.withdraw(accountNumber, withdrawalAmount,today);

		Assert.assertEquals(txnInfoResponse.getTxnStatus(), TxnStatus.Success.getValue());
		verify(accountRepository, times(1)).updateBalance(accountNumber, withdrawalAmount);
		verify(transactionRepository, times(1)).save(any(Transaction.class));

	}
	@Test
	public void testFindByAccountNumber() throws Exception {
		String accountNumber = "508090345600";
		String accountName = "Antony Nzomo";
		BigDecimal openingBalance = new BigDecimal(500000);
		List<Account> accounts = Arrays.asList(TestUtil.createAccount(accountName, accountNumber, openingBalance));
		
		when(accountRepository.getByAccountNumber(accountNumber)).thenReturn(accounts);
		
		Account account = accountService.findByAccountNumber(accountNumber);

		Assert.assertEquals(accountNumber, account.getAccountNumber());

	}
	@Test
	public void testSave() throws Exception {
		String accountNumber = "508090345600";
		boolean status = true;
		String accountName = "Antony Nzomo";
		BigDecimal openingBalance = new BigDecimal(500000);
		Account account = TestUtil.createAccount(accountName, accountNumber, openingBalance);
		
		when(accountRepository.save(account)).thenReturn(status);
		
		status = accountService.save(account);

		Assert.assertTrue(status);
		verify(accountRepository,times(1)).save(account);

	}
	
	private void assertBalanceResponseEquals(BalanceResponse response, String accountName, String accountNumber,
			BigDecimal amount) {
		Assert.assertEquals(response.getAccountname(), accountName);
		Assert.assertEquals(response.getAccountnumber(), accountNumber);
		Assert.assertEquals(response.getBalance(), amount);
	}

}
