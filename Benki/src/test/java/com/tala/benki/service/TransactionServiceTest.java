package com.tala.benki.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.tala.benki.domain.dto.BalanceResponse;
import com.tala.benki.domain.dto.TxnInfoResponse;
import com.tala.benki.domain.entity.masterdata.Account;
import com.tala.benki.domain.entity.masterdata.Transaction;
import com.tala.benki.enums.TxnStatus;
import com.tala.benki.enums.TxnType;
import com.tala.benki.repository.entity.AccountRepository;
import com.tala.benki.repository.entity.TransactionRepository;
import com.tala.benki.service.entity.AccountService;
import com.tala.benki.service.entity.TransactionService;
import com.tala.benki.service.entity.impl.AccountServiceImpl;
import com.tala.benki.service.entity.impl.TransactionServiceImpl;
import com.tala.benki.util.TestUtil;

@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceTest {
	@Mock
	private TransactionRepository transactionRepository;
	@InjectMocks
	private TransactionService transactionService = new TransactionServiceImpl();

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void testSave() throws Exception {
		String accountNumber = "508090345600";
		boolean status = true;
		String accountName = "Antony Nzomo";
		BigDecimal openingBalance = new BigDecimal(500000);
		BigDecimal depositAmount = new BigDecimal(1000);
		int transactionType = TxnType.Deposit.getValue();
		Account account = TestUtil.createAccount(accountName, accountNumber, openingBalance);
		Transaction txn = TestUtil.createTxn(account.get_id(), depositAmount, transactionType);

		when(transactionRepository.save(txn)).thenReturn(status);

		status = transactionService.save(txn);

		Assert.assertTrue(status);
		verify(transactionRepository, times(1)).save(txn);
	}

	@Test
	public void testUpdate() throws Exception {
		String accountNumber = "508090345600";
		String accountName = "Antony Nzomo";
		BigDecimal openingBalance = new BigDecimal(500000);
		BigDecimal depositAmount = new BigDecimal(1000);
		BigDecimal updatedAmount = new BigDecimal(2000);
		int transactionType = TxnType.Deposit.getValue();
		Account account = TestUtil.createAccount(accountName, accountNumber, openingBalance);
		Transaction txn = TestUtil.createTxn(account.get_id(), depositAmount, transactionType);
		txn.setAmount(updatedAmount);

		when(transactionRepository.update(txn)).thenReturn(txn);

		Transaction updatedTxn = transactionService.update(txn);

		Assert.assertEquals(updatedTxn.getAmount(), updatedAmount);
		verify(transactionRepository, times(1)).update(txn);
	}

	@Test
	public void testGetById() throws Exception {
		String accountNumber = "508090345600";
		String accountName = "Antony Nzomo";
		BigDecimal openingBalance = new BigDecimal(500000);
		BigDecimal depositAmount = new BigDecimal(1000);

		int transactionType = TxnType.Deposit.getValue();
		Account account = TestUtil.createAccount(accountName, accountNumber, openingBalance);
		Transaction txn = TestUtil.createTxn(account.get_id(), depositAmount, transactionType);

		when(transactionRepository.getById(txn.get_id())).thenReturn(txn);

		txn = transactionService.getById(txn.get_id());

		Assert.assertEquals(txn.getAccount(), account.get_id());
		verify(transactionRepository, times(1)).getById(txn.get_id());
	}

	@Test
	public void testGetAll() throws Exception {
		String accountNumber = "508090345600";
		String accountName = "Antony Nzomo";
		BigDecimal openingBalance = new BigDecimal(500000);
		BigDecimal depositAmount = new BigDecimal(1000);

		int transactionType = TxnType.Deposit.getValue();
		Account account = TestUtil.createAccount(accountName, accountNumber, openingBalance);
		List<Transaction> txns = Arrays.asList(TestUtil.createTxn(account.get_id(), depositAmount, transactionType));

		when(transactionRepository.getAll()).thenReturn(txns);

		txns = transactionService.getAll();

		Assert.assertEquals(txns.size(), 1);
		verify(transactionRepository, times(1)).getAll();
	}

	@Test
	public void testDeleteById() throws Exception {
		boolean status = true;
		String accountNumber = "508090345600";
		String accountName = "Antony Nzomo";
		BigDecimal openingBalance = new BigDecimal(500000);
		BigDecimal depositAmount = new BigDecimal(1000);

		int transactionType = TxnType.Deposit.getValue();
		Account account = TestUtil.createAccount(accountName, accountNumber, openingBalance);
		Transaction txn = TestUtil.createTxn(account.get_id(), depositAmount, transactionType);

		when(transactionRepository.deleteById(txn.get_id())).thenReturn(status);

		status = transactionService.deleteById(txn.get_id());

		Assert.assertTrue(status);
		verify(transactionRepository, times(1)).deleteById(txn.get_id());
	}

	@Test
	public void testDeleteAll() throws Exception {
		boolean status = true;

		when(transactionRepository.deleteAll()).thenReturn(status);

		status = transactionService.deleteAll();

		Assert.assertTrue(status);
		verify(transactionRepository, times(1)).deleteAll();
	}
}
