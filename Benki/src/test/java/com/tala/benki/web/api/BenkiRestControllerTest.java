package com.tala.benki.web.api;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tala.benki.config.WebConfig;
import com.tala.benki.domain.dto.TxnInfoResponse;
import com.tala.benki.domain.entity.masterdata.Account;
import com.tala.benki.enums.TxnStatus;
import com.tala.benki.service.entity.AccountService;
import com.tala.benki.service.entity.TransactionService;
import com.tala.benki.util.TestUtil;
import com.tala.benki.web.filter.CORSFilter;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { WebConfig.class })
public class BenkiRestControllerTest {

	private MockMvc mockMvc;

	@Mock
	private AccountService accountService;

	@Mock
	private TransactionService transactionService;

	@InjectMocks
	private BenkiRestController benkiRestController;

	@Before
	public void init() throws Exception {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(benkiRestController).addFilters(new CORSFilter()).build();
	}

	@Test
	public void testGetAccountBalance() throws Exception {

		String accountNumber = "508090345600";
		String accountName = "Antony Nzomo";
		BigDecimal balance = new BigDecimal(500000);

		when(accountService.getBalance(accountNumber))
				.thenReturn(TestUtil.createBalanceResponse(balance, accountNumber, accountName));

		mockMvc.perform(get("/benki/account/{accno}", accountNumber)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(content().string(
						"{\"balance\":500000,\"accountnumber\":\"508090345600\",\"accountname\":\"Antony Nzomo\"}"));

		verify(accountService, times(1)).getBalance(accountNumber);
	}

	@Test
	public void testCreateAccount() throws Exception {

		String accountNumber = "508090345600";
		String accountName = "Antony Nzomo";
		BigDecimal balance = new BigDecimal(500000);
		boolean status = true;
		Account account = TestUtil.createAccount(accountName, accountNumber, balance);

		when(accountService.findByAccountNumber(account.getAccountNumber())).thenReturn(null);
		when(accountService.save(any(Account.class))).thenReturn(status);

		mockMvc.perform(post("/benki/account").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content(asJsonString(account))).andExpect(status().isCreated())
				.andExpect(content().string(
						"{\"errorInfo\":\"Account successfully created\",\"txnStatus\":\"Successful\",\"txnId\":0}"));
		
		verify(accountService, times(1)).findByAccountNumber(accountNumber);
		verify(accountService, times(1)).save(any(Account.class));
	}

	@Test
	public void testDeposit() throws Exception {

		String accountNumber = "508090345600";
		BigDecimal amount = new BigDecimal(500000);
		Date today = new Date();

		TxnInfoResponse response = new TxnInfoResponse(
				"Deposit Amount worth of $" + amount + ", to Account Number: " + accountNumber,
				TxnStatus.Success.getValue());

		when(accountService.deposit(accountNumber, amount, today)).thenReturn(response);

		mockMvc.perform(
				put("/benki/deposit/{accno}/{amount}", accountNumber, amount).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());				
		
		verify(accountService, times(1)).deposit(any(String.class),any(BigDecimal.class),any(Date.class));
	}

	@Test
	public void testWithdraw() throws Exception {

		String accountNumber = "508090345600";
		BigDecimal amount = new BigDecimal(500000);
		Date today = new Date();

		TxnInfoResponse response = new TxnInfoResponse(
				"Withdraw Amount worth of $" + amount + ", from Account Number: " + accountNumber,
				TxnStatus.Success.getValue());

		when(accountService.withdraw(accountNumber, amount, today)).thenReturn(response);

		mockMvc.perform(
				put("/benki/withdraw/{accno}/{amount}", accountNumber, amount).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
		
		verify(accountService, times(1)).withdraw(any(String.class),any(BigDecimal.class),any(Date.class));
	}

	private static String asJsonString(final Object obj) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
