package com.tala.benki.repository;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.tala.benki.domain.entity.masterdata.Account;
import com.tala.benki.enums.EntityStatus;
import com.tala.benki.enums.TxnType;
import com.tala.benki.exception.AccountNotFoundException;
import com.tala.benki.repository.entity.AccountRepository;
import com.tala.benki.repository.entity.TransactionRepository;
import com.tala.benki.repository.entity.impl.AccountRepositoryImpl;
import com.tala.benki.repository.entity.impl.TransactionRepositoryImpl;
import com.tala.benki.util.IDUtils;

@RunWith(MockitoJUnitRunner.class)
public class TransactionRepositorTest {
	@Mock
	private EntityManager entityManager;
	@InjectMocks
	private TransactionRepository transactionRepository = new TransactionRepositoryImpl();

	private static final String SELECT_TXN_AMOUNT_BY_DATE_QUERY = "SELECT SUM(t.amount) FROM Transaction t WHERE t.dateCreated = :dateCreated AND t.account = :account AND t.transactionType = :transactionType";
	private static final String SELECT_TXN_COUNT_BY_DATE_QUERY = "SELECT COUNT(t._id) FROM Transaction t WHERE t.dateCreated = :dateCreated AND t.account = :account AND t.transactionType = :transactionType";

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void testGetCumulativeTxnAmountForTheDay() throws Exception {
		Date dateCreated = new Date();
		long accountId = IDUtils.generateRandom();
		int transactionType = TxnType.Deposit.getValue();
		Object[] transactions = new String[] { "15000" };

		Query query = mock(Query.class);
		when(query.setParameter("dateCreated", dateCreated)).thenReturn(query);
		when(query.setParameter("account", accountId)).thenReturn(query);
		when(query.setParameter("transactionType", transactionType)).thenReturn(query);

		when(query.getSingleResult()).thenReturn(transactions);
		when(entityManager.createNativeQuery(SELECT_TXN_AMOUNT_BY_DATE_QUERY)).thenReturn(query);
		// retrieve transaction total amount for the day
		BigDecimal txnAmount = transactionRepository.getCumulativeTxnAmountForTheDay(dateCreated, accountId,
				transactionType);

		Assert.assertEquals(txnAmount, new BigDecimal("15000"));

	}

	@Test
	public void testGetDailyTransactionCount() throws Exception {
		Date dateCreated = new Date();
		long accountId = IDUtils.generateRandom();
		int transactionType = TxnType.Deposit.getValue();
		Object[] transactions = new Integer[] { 3 };

		Query query = mock(Query.class);
		when(query.setParameter("dateCreated", dateCreated)).thenReturn(query);
		when(query.setParameter("account", accountId)).thenReturn(query);
		when(query.setParameter("transactionType", transactionType)).thenReturn(query);

		when(query.getSingleResult()).thenReturn(transactions);
		when(entityManager.createNativeQuery(SELECT_TXN_COUNT_BY_DATE_QUERY)).thenReturn(query);
		// retrieve daily transaction count
		int txnCount = transactionRepository.getDailyTransactionCount(dateCreated, accountId, transactionType);

		Assert.assertEquals(txnCount, 3);

	}

	@Test(expected = Exception.class)
	public void testGetCumulativeTxnAmountForTheDay_WithException() throws Exception {
		Date dateCreated = new Date();
		long accountId = IDUtils.generateRandom();
		int transactionType = TxnType.Deposit.getValue();
		Object[] transactions = null;

		Query query = mock(Query.class);
		when(query.setParameter("dateCreated", dateCreated)).thenReturn(query);
		when(query.setParameter("account", accountId)).thenReturn(query);
		when(query.setParameter("transactionType", transactionType)).thenReturn(query);

		when(query.getSingleResult()).thenReturn(transactions);
		when(entityManager.createNativeQuery(SELECT_TXN_AMOUNT_BY_DATE_QUERY)).thenReturn(query);
		// generate exception by null object array
		BigDecimal txnAmount = transactionRepository.getCumulativeTxnAmountForTheDay(dateCreated, accountId,
				transactionType);

		verify(transactionRepository, times(1)).getCumulativeTxnAmountForTheDay(dateCreated, accountId,
				transactionType);
		Assert.assertNull(txnAmount);
	}

	@Test(expected = Exception.class)
	public void testGetDailyTransactionCount_WithException() throws Exception {
		Date dateCreated = new Date();
		long accountId = IDUtils.generateRandom();
		int transactionType = TxnType.Deposit.getValue();
		Object[] transactions = null;

		Query query = mock(Query.class);
		when(query.setParameter("dateCreated", dateCreated)).thenReturn(query);
		when(query.setParameter("account", accountId)).thenReturn(query);
		when(query.setParameter("transactionType", transactionType)).thenReturn(query);

		when(query.getSingleResult()).thenReturn(transactions);
		when(entityManager.createNativeQuery(SELECT_TXN_COUNT_BY_DATE_QUERY)).thenReturn(query);
		// retrieve daily transaction count
		int txnCount = transactionRepository.getDailyTransactionCount(dateCreated, accountId, transactionType);

		Assert.assertEquals(txnCount, 3);
	}

}
