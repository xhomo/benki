package com.tala.benki.repository;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.tala.benki.domain.entity.masterdata.Account;
import com.tala.benki.exception.AccountNotFoundException;
import com.tala.benki.repository.entity.AccountRepository;
import com.tala.benki.repository.entity.impl.AccountRepositoryImpl;
import com.tala.benki.util.TestUtil;

@RunWith(MockitoJUnitRunner.class)
public class AccountRepositorTest {
	@Mock
	private EntityManager entityManager;
	@InjectMocks
	private AccountRepository accountRepository = new AccountRepositoryImpl();

	private static final String FIND_BY_ACCNO_QUERY = "Account.findByAccountNo";

	@Before
	public void setUp() throws Exception {

	}

	@SuppressWarnings("unchecked")
	@Test
	public void testGetByAccountNumber() throws AccountNotFoundException, Exception {
		String accountNumber = "508090345600";
		BigDecimal openingBalance = new BigDecimal(500000);
		List<Account> accounts = Arrays.asList(TestUtil.createAccount("Antony Nzomo", accountNumber, openingBalance));

		Query query = mock(TypedQuery.class);
		when(query.setParameter("accNo", accountNumber)).thenReturn(query);
		when(query.getResultList()).thenReturn(accounts);
		when(entityManager.createNamedQuery(FIND_BY_ACCNO_QUERY, Account.class))
				.thenReturn((TypedQuery<Account>) query);

		List<Account> list = accountRepository.getByAccountNumber(accountNumber);
		Assert.assertEquals(1, list.size());
		Assert.assertEquals(accountNumber, list.get(0).getAccountNumber());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testUpdateBalance() throws AccountNotFoundException, Exception {
		String accountNumber = "508090345600";
		BigDecimal openingBalance = new BigDecimal(500000);
		BigDecimal deposit = new BigDecimal(50000);
		List<Account> accounts = Arrays.asList(TestUtil.createAccount("Antony Nzomo", accountNumber, openingBalance));

		Query query = mock(TypedQuery.class);
		when(query.setParameter("accNo", accountNumber)).thenReturn(query);
		when(query.getResultList()).thenReturn(accounts);
		when(entityManager.createNamedQuery(FIND_BY_ACCNO_QUERY, Account.class))
				.thenReturn((TypedQuery<Account>) query);
		when(entityManager.merge(accounts.get(0))).thenReturn(accounts.get(0));

		Account updatedAccount = accountRepository.updateBalance(accountNumber, deposit);
		Assert.assertEquals(updatedAccount.getBalance(), openingBalance.add(deposit));
	}

	@SuppressWarnings("unchecked")
	@Test(expected = AccountNotFoundException.class)
	public void testGetByAccountNumber_WithAccountNotFoundException() throws AccountNotFoundException, Exception {
		String accountNumber = "508090345600";
		List<Account> accounts = new ArrayList<Account>();

		Query query = mock(TypedQuery.class);
		when(query.setParameter("accNo", accountNumber)).thenReturn(query);
		when(query.getResultList()).thenReturn(accounts);
		when(entityManager.createNamedQuery(FIND_BY_ACCNO_QUERY, Account.class))
				.thenReturn((TypedQuery<Account>) query);

		List<Account> list = accountRepository.getByAccountNumber(accountNumber);
		Assert.assertEquals(1, list.size());
		Assert.assertEquals(accountNumber, list.get(0).getAccountNumber());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = AccountNotFoundException.class)
	public void testUpdateBalance_WithAccountNotFoundException() throws AccountNotFoundException, Exception {
		String accountNumber = "508090345600";
		BigDecimal depositAmount = new BigDecimal(50000);
		List<Account> accounts = new ArrayList<>();

		Query query = mock(TypedQuery.class);
		when(query.setParameter("accNo", accountNumber)).thenReturn(query);
		when(query.getResultList()).thenReturn(accounts);
		when(entityManager.createNamedQuery(FIND_BY_ACCNO_QUERY, Account.class))
				.thenReturn((TypedQuery<Account>) query);

		Account updatedAccount = accountRepository.updateBalance(accountNumber, depositAmount);
		verify(accountRepository, times(1)).updateBalance(accountNumber, depositAmount);
		Assert.assertEquals(updatedAccount, null);
	}

	

}
