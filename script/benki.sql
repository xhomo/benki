CREATE DATABASE IF NOT EXISTS benki;

USE benki;

CREATE TABLE IF NOT EXISTS account(
id BIGINT PRIMARY KEY,
name VARCHAR(225) NOT NULL,
acc_no VARCHAR(50) NOT NULL,
book_balance DECIMAL(13,2), 
date_created DATE NOT NULL,
date_last_updated DATE NOT NULL,
description VARCHAR(225),
status INT(2)
);

CREATE TABLE IF NOT EXISTS acc_txn(
id BIGINT PRIMARY KEY,
date_created DATE,
date_last_updated DATE NOT NULL,
description VARCHAR(225),
account BIGINT NOT NULL,
amount DECIMAL(13,2),
status INT(2),
txn_type INT(2),
FOREIGN KEY(account) REFERENCES account(id)
);

